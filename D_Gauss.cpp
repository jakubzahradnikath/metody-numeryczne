﻿#include "stdafx.h"
#include <iostream>
#include <math.h>


using namespace std;

int i, j, k, n, a;
double mx, s, m;

double gauss(double **tab, int n)
{
	double *temp = new double [n+1];
	for (k = 0; k < n; k++)
	{
		mx = tab[k][k];
		if (mx == 0)
		{
			m = mx;
			a = k;
			for (i = k; i < n; i++)
			{
				if (abs(tab[i][k]) > m) 
				{
					m = abs(tab[i][k]);
					a = i;
				}
			}
			for (j = k; j <= n; j++)
			{
				temp[j] = tab[k][j];
				tab[k][j] = tab[a][j];
				tab[a][j] = temp[j];
				mx = tab[k][k];
			}
		}
		else
		{
			m = 1;
		}
		for (j = k; j <= n; j++)
		{
			tab[k][j] = tab[k][j]/mx;
		}
		for (i = 0; i < n; i++)
		{
			if (i != k)
			{
				s = tab[i][k];
				for (j = k; j <= n; j++)
				{
					tab[i][j] = tab[i][j] - s * tab[k][j];
				}
			}
		}
	}
	delete [] temp;
	return **tab;
}


int main()
{
	cout << "Podaj n: " << endl;
	cout << endl;
	cin >> n;
	

	double **tab = new double *[n];
	for (i = 0; i < n; i++)
	
	{
		tab[i] = new double [n+1];
	}

	
	for (i = 0; i < n; i++)
	{
		for (j = 0; j <= n; j++)
		{
			cout << "Podaj wspolczynniki macierzy [" << i+1 << "][" << j + 1 << "]: ";
			cin >> tab[i][j];
		}
	}
	
	gauss(tab, n);
	
	if (m == 0)
	{
		cout<<"macierz jest osobliwa, brak rozwiazan"<<endl;
	}
	else
	{
		for (i = 0; i < n; i++) 
		{
			cout << "x" << i+1 << " = " << tab[i][n] << endl;
		}
	}

	for (i = 0; i < n; i++)
	{
		delete [] tab[i];
	}
	delete [] tab;

	return 0;
}