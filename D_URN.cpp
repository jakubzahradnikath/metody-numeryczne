#include "stdafx.h"
#include <iostream>
#include <conio.h>
using namespace std;

class URN
{
	double x, y, f, g, epsX = 1e-8, epsF = epsX, gx, gy, fx, fy;
	double W, Wx, Wy, hx, hy, xp, yp;
	int iteracje = 0;
	int p�tla = 0;

	void PodajDane();
	void Oblicz();
	void Wynik();
public:
	void Start();
};

void URN::PodajDane()
{
	cout << "Podaj X poczatkowe " << endl;
	cin >> x;
	cout << "Podaj Y poczatkowe" << endl;
	cin >> y;
}

void URN::Oblicz()
{
	do {
		//pochodne
		fx = (3 * x*x) * (y*y) + y + (y*y*y);
		fy = (x*x*x) * (2 * y) + x + (x*(3 * y*y));
		gx = (2 * x) + (2 * x)*(y*y) - (2 * y);
		gy = (x*x) * (x*x)*(2 * y) - (2 * x);

		//funkcje
		f = (x*x*x)*(y*y) + (x*y) + (x*y*y*y) - 3;
		g = (x*x) + (x*x*y*y) - (2 * x*y);

		W = -gx*fy + fx*gy;
		Wx = g*fy - f * gy;
		Wy = f*gx - g*fx;
		hx = Wx / W;
		hy = Wy / W;

		xp = x;
		yp = y;
		x = xp + hx;
		y = yp + hy;

		iteracje++;

		if (abs(hx) < epsX && abs(hy) < epsX)
		{
			if (abs(f) < epsF && abs(g) < epsF)
			{
				p�tla = 1;
			}
		}
	} while (p�tla == 0);
}

void URN::Wynik()
{
	system("cls");
	cout << "X = " << x << endl;
	cout << "Y = " << y << endl;
	cout << "liczba iteracji = " << iteracje << endl;
}

void URN::Start()
{
	PodajDane();
	Oblicz();
	Wynik();
	_getch();
}

int main()
{
	URN urn;
	urn.Start();
}

