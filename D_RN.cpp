#include "stdafx.h"
#include <iostream>
#include <cmath>
#include <string>
#include <conio.h>

using namespace std;

double f(double a)
{
	return 3 * a + sin(a) - exp(a);
}
int main()
{
	double x0 = 0, x1 = 1, x2, i = 0, eps = 0.000001;
	
	do
	{
		x2 = x1 - ((f(x1)*(x1 - x0)) / (f(x1) - f(x0)));
		if (x2 <= 0)
		{
			x2 = eps;
			x1 = -1;
		}
		else
		{
			x1 = x0;
			x0 = x2;
			i++;
		}
		cout.precision(15);
		cout << "iteracja: " << i << " x = " << x0 << endl;

	} while (fabs(x2 - x0) <= eps);
	_getch();
}

