#include "stdafx.h"
#include <iostream>
#include <iomanip>
#include <cmath>
#include <conio.h>

using namespace std;

const double EPSILON = 1e-38; //PRZYBLIZENIE 0 dla typu double, zmienna globalna
						  
bool Metoda_GaussaJordana(int n, double ** MacierzAB, double * MacierzX)
{

	double m, s;

	for (int i = 0; i < n - 1; i++)
	{
		for (int  j = i + 1; j < n; j++)
		{
			if (fabs(MacierzAB[i][i]) < EPSILON) return false;  //jesli mniejsze niz epsilon, czyli w okolicy 0 zwroc false
			m = -MacierzAB[j][i] / MacierzAB[i][i];
			for (int k = i + 1; k <= n; k++)
			{
				MacierzAB[j][k] += m * MacierzAB[i][k];
			}
		}
	}

	for (int i = n - 1; i >= 0; i--)
	{
		s = MacierzAB[i][n];
		for (int j = n - 1; j >= i + 1; j--)
		{
			s -= MacierzAB[i][j] * MacierzX[j];
		}
		if (fabs(MacierzAB[i][i]) < EPSILON)
		{
			return false;
		}
		MacierzX[i] = s / MacierzAB[i][i];
	}
	return true;
}

int main()
{
	double **MacierzAB, *MacierzX; //do tablic dyn
	int      n; // ilo�� r�wna�

	cout << setprecision(15) << fixed; //ustawienie precyzji

	cout << "Podaj liczbe rownan\n";
	cin >> n;

	//tworzenie tablic o podanym rozmiarze
	MacierzAB = new double *[n];
	MacierzX = new double[n];
	system("cls");

	cout << "Podaj wspolczynniki oddzielajac je spacja. Zatwierzenie wiersza nastepuje po ENTER\n";
	for (int i = 0; i < n; i++)
	{
		MacierzAB[i] = new double[n + 1];
	}
	
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j <= n; j++) cin >> MacierzAB[i][j];
	}

	system("cls");

	if (Metoda_GaussaJordana(n, MacierzAB, MacierzX))
	{
		for (int i = 0; i < n; i++)
		{
			cout << "x" << i + 1 << " = " << setw(9) << MacierzX[i] << endl;
		}
	}
	else
	{
		cout << "MACIERZ OSOBLIWA\n";
		cout << "Enter zakonczy program\n";
		_getch();
		exit(0);
	}

	for (int i = 0; i < n; i++) delete[] MacierzAB[i];
	delete[] MacierzAB;
	delete[] MacierzX;

	_getch();
	return 0;
}
