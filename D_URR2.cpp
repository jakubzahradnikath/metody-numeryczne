#include "stdafx.h"
#include <iostream>


using namespace std;

double f1(double t, double x, double y)
{
		return (-1)*(x/t) + y;
}
double f2(double t, double x, double y)
{	
		return (-2)*(x/(t*t)) + y/t;
}


int main()
{
	double k11,k12,k21,k22,k31,k32,k41,k42;
	double h,t0,t,x,y,N,t1;
	

	
	cout<<"Podaj t0: ";
	cin>>t0;
	cout<<"Podaj t: ";
	cin>>t;
	cout<<"Podaj x("<<t0<<"): ";
	cin>>x;
	cout<<"Podaj y("<<t0<<"): ";
	cin>>y;
	cout<<"Podaj N: ";
	cin>>N;
	t1=t0;
	
	/*
	N=1000;
	t0=1;
	double t1=1;
	t=3;
	
	x=1;
	y=1;
	*/
	h=(t-t0)/N;
	
	for (int i=1;i<=N;i++)
	{

		k11 = h * f1(t1, x, y);
		k12 = h * f2(t1, x, y);

		k21 = h * f1(t1+0.5*h, x+0.5*k11, y+0.5*k11);
		k22 = h * f2(t1+0.5*h, x+0.5*k12, y+0.5*k12);

		k31 = h * f1(t1+0.5*h, x+0.5*k21, y+0.5*k21);
		k32 = h * f2(t1+0.5*h, x+0.5*k22, y+0.5*k22);

		k41 = h * f1(t1+h, x+k31, y+k31);
		k42 = h * f2(t1+h, x+k32, y+k32);

		x+=(((1.0)/(6.0))*(k11 + 2*k21 + 2*k31 + k41));
		y+=(((1.0)/(6.0))*(k12 + 2*k22 + 2*k32 + k42));

		t1=t0+i*h;

	}	
	
	cout<<"------------------------"<<endl;
	cout<<"t= "<<t1<<endl;
	cout<<"x("<<t<<")= "<<x<<endl;
	cout<<"y("<<t<<")= "<<y<<endl;
	cout<<"Wartosc kroku: "<<h<<endl;	

	return 0;
}