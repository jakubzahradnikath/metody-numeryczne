#include "stdafx.h"
#include <iostream>
#include <math.h>

using namespace std;

int i, j, k, n, a;
double mx, s, m,x,y;

double podaj(int m, int n, double *xx, double *yy, double **A)
{
		for (i = 0; i < n; i++)
	{
		cout << "Podaj x" << i << ": ";
		cin >> xx[i];
		cout << "Podaj y" << i << ": ";
		cin >> yy[i];
	}


	for (i = 0; i <= m; i++)
	{
		for (j = 0; j <= m; j++)
		{
			double ad = 0;
			double ij = (double)(i+j);
			for (k = 0; k < n; k++)
			{
				ad += pow(xx[k], ij);
			}
			A[i][j] = ad;
			
		}
	}

	for (i = 0; i <= m; i++)
	{
		double ap = 0,ar=0;
		for (k = 0; k < n; k++)
		{   
			ar = yy[k] * pow(xx[k], i);
			ap += ar;
		}
		A[i][m+1] = ap;
	}

	return **A;
}

double gauss(double **tab, int n)
{
	double *temp = new double [n+1];
	for (k = 0; k <= n; k++)
	{
		mx = tab[k][k];
		if (mx == 0)
		{
			m = mx;
			a = k;
			for (i = k; i <= n; i++)
			{
				if (abs(tab[i][k]) > m) 
				{
					m = abs(tab[i][k]);
					a = i;
				}
			}
			for (j = k; j <= n+1; j++)
			{
				temp[j] = tab[k][j];
				tab[k][j] = tab[a][j];
				tab[a][j] = temp[j];
				mx = tab[k][k];
			}
		}
		else
		{
			m = 1;
		}
		for (j = k; j <= n+1; j++)
		{
			tab[k][j] = tab[k][j]/mx;
		}
		for (i = 0; i < n; i++)
		{
			if (i != k)
			{
				s = tab[i][k];
				for (j = k; j <= n+1; j++)
				{
					tab[i][j] = tab[i][j] - s * tab[k][j];
				}
			}
		}
	}
	delete [] temp;
	return **tab;
}
double interpol(double *B, double *A,int n,double x )
{
	double *ui = new double [n];
	for (i = 0; i < n; i++)
	{
		if (i == 0)
		{
			ui[i] = 1;
		}
		else if (i == 1)
		{
			ui[i]=1-x;
		}
		else
		{
			double w=i;
			ui[i] = (((2*w-1-x)/w) * ui[i-1]) - ((w-1)/w * ui[i-2]);
		}
		A[i] = B[i] * ui[i];
	}
	delete [] ui;
	return *A;
}

double aproks(double *B, double x, int m)
{
	double P = B[m] * pow(x, m);
	return P;
}
int main()
{
	int m;
	cout << "Podaj n: ";
	cin >> n;
	cout << "Podaj k: ";
	cin >> m;

	double *xx = new double[n];
	double *yy = new double[n];


	double **U = new double *[m+1];
	for (i = 0; i <= m; i++)
	{
		U[i] = new double [m+2];
	}
	double *B = new double [m+1];
	podaj(m, n, xx, yy, U);
	gauss(U, m);
	for (i = 0; i <= m; i ++)
	{
		B[i] = U[i][m+1];
		cout<<"a"<<i<<"="<<B[i]<<endl;
	}
	cout<<endl;
	

	double z=0,t=0,sum=0;
	for (i = 0; i < n; i++)
	{
		for (j = 0; j <= m; j++)
		{
			 z = aproks(B, xx[i], j);
			 t += z;
		}
		sum += pow( (t - yy[i]), 2);
	}
	cout<<"Wynik po aproksymacji:" << sum << endl;
	return 0;
}