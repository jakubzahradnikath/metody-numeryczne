#include "stdafx.h"
#include <iostream>
#include <math.h>

using namespace std;

int i, j, k, n, a;
double mx, s, m,x,y;

double podaj(double **U,int n)
{

	for (int i=0;i<n;i++)
	{
		U[i][0]=1;
		cout<<"Podaj x"<<i<<":";
		cin>>U[i][1];
		cout<<"Podaj y"<<i<<":";
		cin>>U[i][n];
	}

	for (int i=0;i<n;i++)
	{
		for (int j=1;j<n;j++)
		{
			U[i][j]=U[i][j-1]*U[i][1];
		}
	}
	

	return **U;

}

double gauss(double **tab, int n)
{
	double *temp = new double [n+1];
	for (k = 0; k < n; k++)
	{
		mx = tab[k][k];
		if (mx == 0)
		{
			m = mx;
			a = k;
			for (i = k; i < n; i++)
			{
				if (abs(tab[i][k]) > m) 
				{
					m = abs(tab[i][k]);
					a = i;
				}
			}
			for (j = k; j <= n; j++)
			{
				temp[j] = tab[k][j];
				tab[k][j] = tab[a][j];
				tab[a][j] = temp[j];
				mx = tab[k][k];
			}
		}
		else
		{
			m = 1;
		}
		for (j = k; j <= n; j++)
		{
			tab[k][j] = tab[k][j]/mx;
		}
		for (i = 0; i < n; i++)
		{
			if (i != k)
			{
				s = tab[i][k];
				for (j = k; j <= n; j++)
				{
					tab[i][j] = tab[i][j] - s * tab[k][j];
				}
			}
		}
	}
	delete [] temp;
	return **tab;
}
double interpol(double *B, double *A,int n,double x )
{
	double *ui = new double [n];
	for (i = 0; i < n; i++)
	{
		if (i == 0)
		{
			ui[i] = 1;
		}
		else
		{
			ui[i] = x * ui[i-1];
		}
		A[i] = B[i] * ui[i];
	}
	return *A;
}

int main()
{
	cout << "Podaj i " << endl;
	cout << endl;
	cin >> n;
	
	double **U = new double *[n];

	for (i = 0; i <= n; i++)
	{
		U[i] = new double [n+1];
	}

	double *B = new double [n];
	double *A = new double [n];
	
	podaj(U, n);
	
	gauss(U, n);

	for (int i=0;i<n;i++)
	{
		B[i] = U[i][n];
	}

	

	
	cout<<endl;
	cout<<"Podaj x:";
	cin>>x;
	interpol(B,A,n,x);
	 for (int i = 0; i < n; i++)
        {
                y += A[i];
        }
        
	cout<<"dla x= "<<y<<" "<<endl;

	return 0;
}