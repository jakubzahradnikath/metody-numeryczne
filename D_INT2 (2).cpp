#include "stdafx.h"
#include <iostream>

using namespace std;

double licz(int n, double x)
{
	if (n >= 2)
	{
		return (((((5.0*n) / (n + 1)) + 2 * x)*(licz(n - 1, x)) + ((x - 2)*licz(n - 2, x))));
	}
	else if (n == 1)
		return (2*x);
	else
		return 1;
}


int main()
{
	int wezly;
	cout << "Podaj ilosc wezlow: ";
	cin >> wezly;

	double** t = new double*[wezly];
	for (int i = 0; i < wezly; ++i)
		t[i] = new double[3];

	cout << "Podaj tabele X,Y" << endl;
	for (int i = 0; i < wezly; i++)
	{
		for (int j = 0; j < 2; j++)
		{
			cin >> t[i][j];
		}
	}

	double* a = new double[wezly];

	int n = wezly;

	double** tab = new double*[n];
	for (int i = 0; i < n; ++i)
		tab[i] = new double[n + 1];

	for (int i = 0; i < n; i++)
	{
		cout << "Rownanie: " << i + 1 << endl;
		for (int j = 0; j < n; j++)
		{
			tab[i][j] = licz(j, t[i][0]);
			cout << tab[i][j] << "    ";
		}
		tab[i][n] = t[i][1];
		cout << tab[i][n] << endl;
	}

	for (int k = 0; k < n; k++)
	{
		double max = tab[k][k];
		int r = k;
		for (int i = k; i < n; i++)
		{
			if (abs(tab[i][k]) > abs(max))
			{
				max = tab[i][k];
				r = i;
			}
		}
		if (max == 0)
		{
			cout << "Macierz ukladu osobliwa" << endl;
			for (int i = 0; i < n; i++)
			{
				cout << "Rownanie: " << i + 1 << endl;
				for (int j = 0; j < n; j++)
				{
					cout << tab[i][j] << "    ";
				}
				cout << tab[i][n] << endl;
			}
			system("pause > nul");
			return 0;
		}
		for (int j = k; j < n + 1; j++)
		{
			double temp = tab[k][j];
			tab[k][j] = tab[r][j];
			tab[r][j] = temp;
		}
		for (int j = k; j < n + 1; j++)
		{
			tab[k][j] /= max;
		}
		for (int i = 0; i < n; i++)
		{
			if (i != k) {
				double b = tab[i][k];
				for (int j = k; j < n + 1; j++)
				{
					tab[i][j] = tab[i][j] - b * tab[k][j];
				}
			}
		}
	}
	for (int i = 0; i < n; i++)
	{
		a[i] = tab[i][n];
		cout << a[i] << endl;
	}
	cout << "Podaj x: ";
	double x;
	cin >> x;
	double suma = 0;
	for (int i = 0; i < n; i++)
	{
		suma += a[i] * licz(i, x);
	}
	cout << suma;
	system("pause > nul");

	return 0;
}

