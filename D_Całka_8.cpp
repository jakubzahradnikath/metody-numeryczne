#include "stdafx.h"
#include <iostream>
#include <cmath>
#include <conio.h>

using namespace std;

int iteracje = 0;

class CALKA
{
	double ktemp = 0;

	double e = 1e-5;
	double FunkcjaZad8(double x);
	double MetodaTrapezow(double a, double b, double e);
	double MetodaSimpsona(double a, double b, double e);
	double MetodaRomberga(double a, double b, double e);

public:
	void START();
};

int main()
{
	CALKA calka;
	calka.START();
    return 0;
}

double CALKA::FunkcjaZad8(double x)
{
	return ((x*x)+1)*exp(-2*x);
}

double CALKA::MetodaTrapezow(double a, double b, double e)
{
	int k = 0;
	double m, h, x, T = 0, temp;
	double SUMA = 0;
	do
	{
		iteracje++;
		SUMA = 0;
		temp = T;
		k++;
		m = pow((double)2, k);
		h = (b - a) / m;
		for (int i = 1; i < m; i++)
		{
			x = a + (i*h);
			SUMA += FunkcjaZad8(x);
		}	
		
		T = (h / 2)*(FunkcjaZad8(a) + FunkcjaZad8(b) + 2 * SUMA);
	} while (abs(T - temp) >= e);
	ktemp = k;
	return T;
}

double CALKA::MetodaSimpsona(double a, double b, double e)
{
	iteracje = 0;
	int k = 0;
	double m, h, x, S = 0, temp;
	double SUMA1 = 0, SUMA2 = 0;
	do
	{
		iteracje++;
		SUMA1 = 0, SUMA2 = 0;
		temp = S;
		k++;
		m = pow((double)2, k);
		h = (b - a) / m;
		for (int i = 1; i < m; i += 2)
		{
			x = a + (i*h);
			SUMA1 += FunkcjaZad8(x);
		}
		for (int i = 2; i < m - 1; i += 2)
		{
			x = a + (i*h);
			SUMA2 += FunkcjaZad8(x);
		}
		S = (h / 3)*(FunkcjaZad8(a) + FunkcjaZad8(a + (m*h)) + 4 * SUMA1 + 2 * SUMA2);
	} while (abs(S - temp) >= e);
	ktemp = k;
	return S;
}

double CALKA::MetodaRomberga(double a, double b, double e)
{
	iteracje = 0;
	int k = 0;
	double m, h, x, R = 0, temp = 0;
	double SUMA = 0;
	if (k == 0)
	{
		R = ((FunkcjaZad8(a) + FunkcjaZad8(b)) / 2)*(b - a);
	}
	do
	{
		iteracje++;
		SUMA = 0;
		temp = R;
		k++;
		m = pow((double)2, k);
		h = (b - a) / m;
		for (int i = 1; i <= pow((double)2, k - 1); i++)
		{
			x = (a + (2 * i - 1) * h);
			SUMA += FunkcjaZad8(x);
		}
		R = ((double)1 / 2)*temp + h * SUMA;
	} while (abs(R - temp) >= e);
	ktemp = k;
	return R;
}

void CALKA::START()
{
	double dolny, gorny;
	cout << "Podaj dolny zakres calkowania a: ";
	cin >> dolny;
	cout << "Podaj gorny zakres calkowania b: ";
	cin >> gorny;

	system("cls");
	cout.precision(5);
	cout << endl;
	cout << "---------------------------------------------" << endl;
	cout << "Metoda tapezow:  " << MetodaTrapezow(dolny, gorny, e) << endl << 
			"Ilosc iteracji:  " << iteracje << endl;
	cout << "---------------------------------------------" << endl << endl;
	cout << "---------------------------------------------" << endl;
	cout << "Metoda Simpsona: " << MetodaSimpsona(dolny, gorny, e) << endl <<
			"Ilosc iteracji:  " << iteracje << endl;
	cout << "---------------------------------------------" << endl << endl;
	cout << "---------------------------------------------" << endl;
	cout << "Metoda Romberga: " << MetodaRomberga(dolny, gorny, e) << endl <<
			"Ilosc iteracji:  " << iteracje << endl;
	cout << "---------------------------------------------" << endl << endl;
	system("pause");
}
