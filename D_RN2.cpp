#include "stdafx.h"
#include <iostream>
#include <math.h>
using namespace std;

double funkcja(double x)
{
	return 2*x-exp(-x);
}

int main()
{

	double a=0;
	double b=0;
	double eps=0;

	cout<<"Podaj poczatek przedziału: ";
	cin>>a;
	cout<<"Podaj koniec przedziału: ";
	cin>>b;
	cout<<"Podaj dokladnosc rozwiazania (epsilon): ";
	cin>>eps;

	double c=0; //wartosc funkcji
	int licznik=1; //licznik krokow
	
	if (funkcja(a) == 0)
	{
		c=funkcja(a);
		
	}
	else 
	{
		licznik++;
		if (funkcja(b)==0)
		{
			c=funkcja(b);
		
		}
		else
		{
			licznik++;
			for (int i=a;abs(b-a)>eps;i++)
			{
				c=a-funkcja(a) * (b-a)/(funkcja(b)-funkcja(a));
				a=b;
				b=c;
				licznik++;
			}
		
		}
	}
	
	cout<<"Pierwiastkiem tego rownania jest: "<<c<<endl;
	cout<<"Dokladnosc rozwiazania: "<<eps<<endl;
	cout<<"Ilosc krokow: "<<licznik<<endl;
	return 0;
}

