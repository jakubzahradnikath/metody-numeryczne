#include "stdafx.h"
#include <iostream>
#include <math.h>
#include <iomanip>

using namespace std;

double f(double x)
{
	return exp(-x);
	//return x;
}
double g(double x, double j)
{
	return f(x) * pow(x,j);
}
double suma(double a, double b, double k)
{
	double xi;
	double h = (b-a)/k;
	double s = 0;
	for (int i=0;i<=k;i++)
	{
		xi = a + i*h;
		s+=g(xi,k);
	}
	return s;
}

double trapez(double a, double b)
{
	double k=1;
	double e = 0.001;
	double hk = (b-a)/pow(2,k);
	double TK = (hk/2)*(g(a,k) + g(b,k) + 2 * suma(a,b,k));
	double TKK;
	do 
	{
		TKK = TK;
		k++;	
		hk = (b-a)/pow(2,k);
		TK = (hk/2)*(g(a,k) + g(b,k) + 2 * suma(a,b,k));
	}
	while (abs(TK - TKK)>=e);
	
	return TK;
}

double calka(double **W,int k,double a, double b)
{
    int tmp=1;
    for(int i=0;i<k;i++)
    {
        for(int j=0;j<k;j++)
        {
            W[i][j]=(pow(b,i+tmp)-pow(a,i+tmp))/(i+tmp);
            tmp++;
        }
        tmp=1;
    }
    return **W;
}

int i, j, k, n, a;
double mx, s, m;

double gauss(double **tab, int n)
{
	double *temp = new double [n+1];
	for (k = 0; k < n; k++)
	{
		mx = tab[k][k];
		if (mx == 0)
		{
			m = mx;
			a = k;
			for (i = k; i < n; i++)
			{
				if (abs(tab[i][k]) > m) 
				{
					m = abs(tab[i][k]);
					a = i;
				}
			}
			for (j = k; j <= n; j++)
			{
				temp[j] = tab[k][j];
				tab[k][j] = tab[a][j];
				tab[a][j] = temp[j];
				mx = tab[k][k];
			}
		}
		else
		{
			m = 1;
		}
		for (j = k; j <= n; j++)
		{
			tab[k][j] = tab[k][j]/mx;
		}
		for (i = 0; i < n; i++)
		{
			if (i != k)
			{
				s = tab[i][k];
				for (j = k; j <= n; j++)
				{
					tab[i][j] = tab[i][j] - s * tab[k][j];
				}
			}
		}
	}
	
	double* tab2 = new double[ n ];
	tab2[ n - 1] = tab[ n - 1 ][ n ] / tab[ n - 1 ][ n - 1 ]; 
	for( int i = 1; i < n; i++ ) 
	{
		tab2[ n - i - 1 ] = tab[ n - i - 1 ][ n ];
		for( int j = 1; j <= i; j++ ) tab2[ n - i - 1 ] -= tab2[ n - j ] * tab[ n - i - 1 ][ n - j ];
	}
	
	cout<<endl;
	for( int i = 0 ; i < n-1 ; i++ )
		cout << "P" << i + 1 << ":" << tab2[ i ] << endl<<endl;
	cout << "S" << tab2[n-1] << endl;
	delete[] tab2;
	



	delete [] temp;
	return **tab;
}



int main()
{
    int k;
    double a,b;
    cout<<"Podaj k: ";
    cin>>k;
	//k++;
    cout<<"Podaj a: ";
    cin>>a;
    cout<<"Podaj b: ";
    cin>>b;

	double **tab = new double *[k];
    
	for (int i = 0; i < k; i++)
        tab[i] = new double [k+1];
	for(int i=0;i<k;i++)
        tab[i][k] = trapez(a,b);
    
	calka(tab,k,a,b);
	for(int i=0;i<k;i++)
	{
		for(int j=0;j<k+1;j++)
		{
			cout<<setprecision(2)<<tab[i][j]<<"\t";
		}
		cout << endl;
	}
	gauss(tab,k);



    

    delete [] tab;
    return 0;
}
