//L_0(x) = 1 L_1(x)=1-x
//L_n(x) = ((2n-1-x)/n)*L_(n-1)(x) - ((n-1)/n)*L_(n-2)(x))  dla n=2,3,...
//
#include "stdafx.h"
#include <iostream>
#include <math.h>


using namespace std;

int i, j, k, n, a;
double mx, s, m,x,y;

double podaj(double **U,int n)
{
	double *ks = new double[n];
	for (int i=0;i<n;i++)
	{
		U[i][0]=1;
		cout<<"Podaj x"<<i<<":";
		cin>>ks[i];
		U[i][1]=1-ks[i];
		cout<<"Podaj y"<<i<<":";
		cin>>U[i][n];
	}

	for (int i=0;i<n;i++)
	{
		for (int j=2;j<n;j++)
		{
			double w=(double)j;
			U[i][j]= ((((2*w)-1-ks[i])/w)*U[i][j-1]) - (((w-1)/w)*U[i][j-2]);
		}
	}

	return **U;

}

double gauss(double **tab, int n)
{
	double *temp = new double [n+1];
	for (k = 0; k < n; k++)
	{
		mx = tab[k][k];
		if (mx == 0)
		{
			m = mx;
			a = k;
			for (i = k; i < n; i++)
			{
				if (abs(tab[i][k]) > m) 
				{
					m = abs(tab[i][k]);
					a = i;
				}
			}
			for (j = k; j <= n; j++)
			{
				temp[j] = tab[k][j];
				tab[k][j] = tab[a][j];
				tab[a][j] = temp[j];
				mx = tab[k][k];
			}
		}
		else
		{
			m = 1;
		}
		for (j = k; j <= n; j++)
		{
			tab[k][j] = tab[k][j]/mx;
		}
		for (i = 0; i < n; i++)
		{
			if (i != k)
			{
				s = tab[i][k];
				for (j = k; j <= n; j++)
				{
					tab[i][j] = tab[i][j] - s * tab[k][j];
				}
			}
		}
	}
	delete [] temp;
	return **tab;
}
double interpol(double *B, double *A,int n,double x )
{
	double *ui = new double [n];
	
	for (i = 0; i < n; i++)
	{
		if (i == 0)
		{
			ui[i] = 1;
		}
		else if (i == 1)
		{
			ui[i]=1-x;
		}
		else
		{
			double w=i;
			ui[i] = (((2*w-1-x)/w) * ui[i-1]) - ((w-1)/w * ui[i-2]);
		}
		A[i] = B[i] * ui[i];
	}

	return *A;
}

int main()
{
	cout << "Podaj i " << endl;
	cout << endl;
	cin >> n;
	n=n+1;
	double **U = new double *[n];

	for (i = 0; i < n; i++)
	{
		U[i] = new double [n+1];
	}

	double *B = new double [n];
	double *A = new double [n];
	
	podaj(U, n);

	gauss(U, n);

	for (int i=0;i<n;i++)
	{
		B[i] = U[i][n];
	}

	cout<<endl;
	cout<<"Podaj x:";
	cin>>x;
	interpol(B,A,n,x);
	y=0;
	 for (int i = 0; i < n; i++)
        {
                y+=A[i];
        }
        
	cout<<"dla x="<<x<<" y="<<y<<" "<<endl;

	return 0;
}