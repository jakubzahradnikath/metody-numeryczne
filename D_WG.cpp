#include "stdafx.h"
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <conio.h>
#include <cmath>

using namespace std;

int main()
{
	//WCZYTANIE MACIERZY
	double macierz[3][3]=
	{
		{3,-4,8},
		{2,-1,5},
		{-5,10,-15}
	};
	
	/*for (int i = 0; i < 3; i++)
	{
		cout << "Wpisz dane do macierzy";
		for (int j = 0; j < 3; j++)
		{
			cin >> macierz[i][j];
		}
	}*/
	//WYPISANIE MACIERZY
	cout << "Twoja macierz\n\n";
	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			cout << macierz[i][j] << " ";
		}
		cout << endl;
	}

	double max, w=0, z = 1;
	int r;

	for (int k = 0; k < 3; k++)
	{
		max = macierz[k][k];
		r = k;
		for (int i = k+1; i < 3; i++)
		{
			
			if (macierz[i][k] > fabs(max))
			{
				max = macierz[i][k];
				r = i;
			}
		}
		if (max==0)
		{
			w = 0;
			cout << "detA = " << 0;
			exit(0);
		}
		else
		{
			if (r == k)
			{
				for (int l = k+1; l < 3; l++)
				{
					for (int m = k; m < 3; m++)
					{
						macierz[l][m] = macierz[l][m] - macierz[l][k] * macierz[k][m] / max;
					}
				}
			}
			else
			{
				z =(-1)*z;
				for (int n = k; n < 3; n++)
				{
					double temp = macierz[k][n];
					macierz[k][n] = macierz[r][n];
					macierz[r][n] = temp;
				}
				for (int l = k + 1; l < 3; l++)
				{
					for (int m = k; m < 3; m++)
					{
						macierz[l][m] = macierz[l][m] - macierz[l][k] * macierz[k][m] / max;
					}
				}
			}
		}
		w = 1.0;
		for (int i = 0; i < 3; i++)
		{
			w = w*macierz[i][i];
		}
		w = w*z;
	}
	
	cout << "detA = " << w << endl;
	cout << "Twoja macierz\n\n";
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			cout << macierz[i][j] << " ";
		}
		cout << endl;
	}
	_getch();
	return 0;
}

