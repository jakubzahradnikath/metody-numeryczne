#include "stdafx.h"
#include <iostream>
#include <math.h>

using namespace std;

double f(double x)
{
	return (x*x + 1) * exp(-2*x);
}

double ktemp=0;

double trapez(double a, double b, double e)
{
	int k=0;
	double m,h,x,T=0,temp;
	double SUMA = 0;
	do
	{
		SUMA=0;
		temp=T;
		k++;
		m = pow((double)2, k);
		h = (b - a)/ m;
		for(int i = 1; i < m; i++)
		{
			x = a + (i*h);
			SUMA += f(x);
		}
		T = (h/2)*(f(a) + f(b) + 2*SUMA);
		
	}
	while(abs(T-temp)>=e);
	ktemp=k;
	return T;
}

double simpson(double a, double b, double e)
{
	int k=0;
	double m,h,x,S=0,temp;
	double SUMA1 = 0, SUMA2 = 0;
	do
	{
		SUMA1=0,SUMA2=0;
		temp=S;
		k++;
		m = pow((double)2, k);
		h = (b - a)/ m;
		for(int i = 1; i < m; i+=2)
		{
			x = a + (i*h);
			SUMA1 += f(x);
		}
		for(int i = 2; i < m-1; i+=2)
		{
			x = a + (i*h);
			SUMA2 += f(x);
		}
		S = (h/3)*(f(a) + f(a+(m*h)) + 4*SUMA1+2*SUMA2);
		
	}
	while(abs(S-temp)>=e);
	ktemp=k;
	return S;
}


double romberg(double a, double b, double e)
{
	int k=0;
	double m,h,x,R=0,temp=0;
	double SUMA = 0;
	if (k==0)
	{
		R = ((f(a)+f(b))/2)*(b-a);
	}
	do
	{
		
		SUMA=0;
		temp=R;
		k++;
		m = pow((double)2, k);
		h = (b - a)/ m;
		for(int i = 1; i <= pow((double)2,k-1); i++)
		{
			x = (a+(2*i - 1) * h);
			SUMA += f(x);
		}
		R = ((double)1/2)*temp + h*SUMA;	
	}
	while(abs(R-temp)>=e);
	ktemp=k;
	return R;
}

int main()
{

	double a,b,e;
	cout<<"Podaj a: ";
	cin>>a;
	cout<<"Podaj b: ";
	cin>>b;
	cout<<"Podaj epsilon: ";
	cin>>e;
	//a=0;
	//b=3;
	//e = pow((double)10,-5);
	
	cout<<"Metoda trapezow:"<<endl;
	cout<<"Wynik:"<<trapez(a,b,e)<<endl;
	cout<<"k="<<ktemp<<endl;
	cout<<"-------------------------"<<endl;
	cout<<"Metoda Simpsona:"<<endl;
	cout<<"Wynik:"<<simpson(a,b,e)<<endl;
	cout<<"k="<<ktemp<<endl;
	cout<<"-------------------------"<<endl;
	cout<<"Metoda Romberga:"<<endl;
	cout<<"Wynik:"<<romberg(a,b,e)<<endl;
	cout<<"k="<<ktemp<<endl;
	return 0;
}

