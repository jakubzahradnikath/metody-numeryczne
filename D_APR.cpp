#include "stdafx.h"
#include <iostream>
#include <conio.h>
#include <iomanip>

using namespace std;

class APR
{
	int ilosc_wezlow, stopien;
	double ** tablica_xy = new double*[ilosc_wezlow];
	
	void Wprowadz_Dane();
public:
	void START();
};

void APR::Wprowadz_Dane()
{
	ilosc_wezlow = 6;
	for (int i = 0; i < ilosc_wezlow; i++)
		tablica_xy[i] = new double[2];

	tablica_xy[0][0] = 1;
	tablica_xy[1][0] = 1.5;
	tablica_xy[2][0] = 2;
	tablica_xy[3][0] = 4;
	tablica_xy[4][0] = 6;
	tablica_xy[5][0] = 7;

	tablica_xy[0][1] = -0.7;
	tablica_xy[1][1] = -0.35;
	tablica_xy[2][1] = -0.05;
	tablica_xy[3][1] = 1.45;
	tablica_xy[4][1] = 3.95;
	tablica_xy[5][1] = 5.35;

	cout << "X: ";
	for (int i = 0; i < ilosc_wezlow; i++)
	{
		cout << setw(8) << tablica_xy[i][0];
	}
	cout << endl;
	cout << "Y: ";
	for (int i = 0; i < ilosc_wezlow; i++)
	{
		cout << setw(8) << tablica_xy[i][1] << "    ";
	}
	cout << endl;
	cout << "Podaj stopien wielomianu: ";
	cin >> stopien;

	int n = stopien + 1;
	double** tab_obliczenia = new double*[n];
	for (int i = 0; i < n; ++i)
		tab_obliczenia[i] = new double[n + 1];

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			double suma = 0;
			for (int g = 0; g < ilosc_wezlow; g++)
			{
				suma += pow(tablica_xy[g][0], j + i);
			}
			tab_obliczenia[i][j] = suma;
		}
		double suma = 0;
		for (int g = 0; g < ilosc_wezlow; g++)
		{
			suma += tablica_xy[g][1] * pow(tablica_xy[g][0], i);
		}
		tab_obliczenia[i][n] = suma;
	}

	for (int k = 0; k < n; k++)
	{
		double max = tab_obliczenia[k][k];
		int r = k;
		for (int i = k; i < n; i++)
		{
			if (abs(tab_obliczenia[i][k]) > abs(max))
			{
				max = tab_obliczenia[i][k];
				r = i;
			}
		}
		if (max == 0)
		{
			cout << "Macierz osobliwa" << endl;
			_getch();
		}
		for (int j = k; j < n + 1; j++)
		{
			double temp = tab_obliczenia[k][j];
			tab_obliczenia[k][j] = tab_obliczenia[r][j];
			tab_obliczenia[r][j] = temp;
		}
		for (int j = k; j < n + 1; j++)
		{
			tab_obliczenia[k][j] /= max;
		}
		for (int i = 0; i < n; i++)
		{
			if (i != k) {
				double b = tab_obliczenia[i][k];
				for (int j = k; j < n + 1; j++)
				{
					tab_obliczenia[i][j] = tab_obliczenia[i][j] - b * tab_obliczenia[k][j];
				}
			}
		}
	}

	for (int i = 0; i < n; i++)
	{
		cout << "x^" << i << " = " << tab_obliczenia[i][n] << endl;
	}

	double S = 0;
	for (int i = 0; i < ilosc_wezlow; i++)
	{
		double pk = 0;
		for (int j = 0; j<stopien; j++)
		{
			pk += tab_obliczenia[j][n] * pow(tablica_xy[i][0], j);
		}
		S += pow((pk - tablica_xy[i][1]), 2);
	}
	cout << "Blad: " << S << endl;
	int podajx;
	cout << "Podaj x: ";
	cin >> podajx;
	double obliczonePK = 0;
	for (int j = 0; j <= stopien; j++)
	{
		obliczonePK += tab_obliczenia[j][n] * pow(podajx, j);
	}
	cout << "Pk(x): " << obliczonePK;

	_getch();
}

void APR::START()
{
	Wprowadz_Dane();
}

int main()
{
	APR apr;
	// podwojne odpalenie aby sprawdzic dla k=5 i k=2
	cout << "Wprowadz 2\n\n";
	apr.START();
	system("cls");
	cout << "Wprowadz 5\n\n";
	apr.START();

}
