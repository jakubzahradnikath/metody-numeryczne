#include "stdafx.h"
#include <iostream>
#include <cmath>
#include <conio.h>
#include <iomanip>

using namespace std;

class trojkatna
{
private:
	int n; //ilosc r�wna�
	int i; //do p�tli for
	
	double *x, *ponizej_przekatnej, *glowna_przekatna, *powyzej_przekatnej, *beta, *gamma, *wyniki_rownan;

public:
	void wprowadzenie_danych();
	void trojkatna_rozwiazanie();

	~trojkatna()
	{
		delete[] x, glowna_przekatna, powyzej_przekatnej, ponizej_przekatnej, beta, gamma, wyniki_rownan;
	}
};

void main()
{
	trojkatna trojkatna_obiekt;

	trojkatna_obiekt.wprowadzenie_danych();
	system("cls");
	trojkatna_obiekt.trojkatna_rozwiazanie();
	_getch();
}

void trojkatna::wprowadzenie_danych()
{
	cout << "Podaj ilosc rownan\n";
	cin >> n;

	ponizej_przekatnej = new double[n - 1];
	glowna_przekatna = new double[n];
	powyzej_przekatnej = new double[n - 1];
	beta = new double[n];
	gamma = new double[n];
	wyniki_rownan = new double[n];
	x = new double[n];

	cout << "Podaj liczby na glownej przekatnej\n";
	for (i = 0; i < n; i++) //g�owna przek�tna
		cin >> glowna_przekatna[i];

	cout << "Podaj liczby powyzej przekatnej nad glowna przekatna\n";
	for (i = 0; i < (n-1); i++) // powy�ej g��wnej
		cin >> powyzej_przekatnej[i];

	cout << "Podaj liczby ponizej przekatnej nad glowna przekatna\n";
	for (i = 1; i < n; i++) //poni�ej g�ownej
		cin >> ponizej_przekatnej[i - 1];

	cout << "Podaj wyniki rownan \n";
	for (i = 0; i < n; i++) // wyniki
		cin >> wyniki_rownan[i];
}

void trojkatna::trojkatna_rozwiazanie()
{
	for (i = 0; i < n; i++)
	{
		if (i == 0)
		{
			beta[0] = glowna_przekatna[0];
			gamma[0] = wyniki_rownan[0] / beta[0];
		}
		else
		{
			beta[i] = glowna_przekatna[i] - ponizej_przekatnej[i - 1] * powyzej_przekatnej[i - 1] / beta[i - 1];
			gamma[i] = (wyniki_rownan[i] - ponizej_przekatnej[i - 1] * gamma[i - 1]) / beta[i];
		}
	}
	x[n - 1] = gamma[n - 1];

	for (i = (n - 2); i >= 0; i--)
	{
		x[i] = gamma[i] - powyzej_przekatnej[i] * x[i + 1] / beta[i];
	}

	cout << "Wyniki:\n";
	for (i = 0; i < n; i++)
	{
		cout << setw(2) << "x[" << i << "] =" << setw(3) << x[i] << endl;
	}
}