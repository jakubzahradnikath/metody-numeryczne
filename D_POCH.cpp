#include "stdafx.h"
#include <algorithm>
#include <iostream>
#include <conio.h>
using namespace std;

class POCH
{
	const double PI = 3.141592653589793;
	const double i = 0;
	const double x_startowe = 1;
	const double h = 1e-6;

	double f(double x);
	double f2(double x, double y, double z);
	double Xi(double x, double i, double h);
	double Yi(double x, double i, double h);

	double RSPy1(double x, double i, double h);
	double RSTy1(double x, double i, double h);
	double RSCy1(double x, double i, double h);

	double RSPy2(double x, double i, double h);
	double RSTy2(double x, double i, double h);
	double RSCy2(double x, double i, double h);

	double RSPy3(double x, double i, double h);
	double RSTy3(double x, double i, double h);
	double RSCy3(double x, double i, double h);

	double poch_1_cz(short int nr, double h, double x, double y, double z);

	void OBLICZ();

public:
	void START();
};

double POCH::f(double x)
{
	return (exp(-0.5*x)*sin(5 * PI*x));
}

double POCH::f2(double x, double y, double z)
{
	return x + (y*y) + (z*z*z) + (x * y*y*z*z*z);
}

double POCH::Xi(double x, double i, double h)
{
	return x + (i*h);
}

double POCH::Yi(double x, double i, double h) 
{
	return f(Xi(x, i, h));
}

double POCH::RSPy1(double x, double i, double h) 
{
	return (-Yi(x, i + 2, h) + 4 * Yi(x, i + 1, h) - 3 * Yi(x, i, h)) / (2 * h);
}

double POCH::RSTy1(double x, double i, double h)
{
	return (3 * Yi(x, i, h) - 4 * Yi(x, i - 1, h) + Yi(x, i - 2, h)) / (2 * h);
}

double POCH::RSCy1(double x, double i, double h)
{
	return (Yi(x, i + 1, h) - Yi(x, i - 1, h)) / (2 * h);
}

double POCH::RSPy2(double x, double i, double h) 
{
	return (-Yi(x, i + 3, h) + 4 * Yi(x, i + 2, h) - 5 * Yi(x, i + 1, h) + 2 * Yi(x, i, h)) / pow(h, 2);
}

double POCH::RSTy2(double x, double i, double h)
{
	return (2 * Yi(x, i, h) - 5 * Yi(x, i - 1, h) + 4 * Yi(x, i - 2, h) - Yi(x, i - 3, h)) / pow(h, 2);
}

double POCH::RSCy2(double x, double i, double h)
{
	return (Yi(x, i + 1, h) - 2 * Yi(x, i, h) + Yi(x, i - 1, h)) / pow(h, 2);
}

double POCH::RSPy3(double x, double i, double h)
{
	return (-3 * Yi(x, i + 4, h) + 14 * Yi(x, i + 3, h) - 24 * Yi(x, i + 2, h) + 18 * Yi(x, i + 1, h) - 5 * Yi(x, i, h)) / (2 * pow(h, 3));
}

double POCH::RSTy3(double x, double i, double h) 
{
	return (5 * Yi(x, i, h) - 18 * Yi(x, i - 1, h) + 24 * Yi(x, i - 2, h) - 14 * Yi(x, i - 3, h) + 3 * Yi(x, i - 4, h)) / (2 * pow(h, 3));
}

double POCH::RSCy3(double x, double i, double h) 
{
	return (Yi(x, i + 2, h) - 2 * Yi(x, i + 1, h) + 2 * Yi(x, i - 1, h) - Yi(x, i - 2, h)) / (2 * pow(h, 3));
}

double POCH::poch_1_cz(short int nr, double h, double x, double y, double z)
{
	double w_ip1, w_im1;
	switch (nr)
	{
	case 1: 
	{
		w_ip1 = f2(x + h, y, z);
		w_im1 = f2(x - h, y, z);
		break;
	}
	case 2:
	{
		w_ip1 = f2(x, y + h, z);
		w_im1 = f2(x, y - h, z);
		break;
	}
	case 3:
	{
		w_ip1 = f2(x, y, z + h);
		w_im1 = f2(x, y, z - h);
		break;
	}
	default: cout << "Wybierz numer zmiennej: 1->x, 2->y, 3->z" << endl; break;
	}
	return (w_ip1 - w_im1)/(2*h);
}

void POCH::OBLICZ()
{
	double RSP1, RSP2, RSP3;
	double RST1, RST2, RST3;
	double RSC1, RSC2, RSC3;
	double w_f2x, w_f2y, w_f2z;

	RSP1 = RSPy1(x_startowe, i, h);
	RSP2 = RSPy2(x_startowe, i, h);
	RSP3 = RSPy3(x_startowe, i, h);

	RST1 = RSTy1(x_startowe, i, h);
	RST2 = RSTy2(x_startowe, i, h);
	RST3 = RSTy3(x_startowe, i, h);

	RSC1 = RSCy1(x_startowe, i, h);
	RSC2 = RSCy2(x_startowe, i, h);
	RSC3 = RSCy3(x_startowe, i, h);

	w_f2x = poch_1_cz(1, 1e-5, 1, 1, 1);
	w_f2y = poch_1_cz(2, 1e-5, 1, 1, 1);
	w_f2z = poch_1_cz(3, 1e-5, 1, 1, 1);

	cout.precision(4);
	cout << fixed;

	cout << "----- Wyniki RSP: -----" << endl;
	cout << "RSP y1 = " << RSP1 << endl;
	cout << "RSP y2 = " << RSP2 << endl;
	cout << "RSP y3 = " << RSP3 << endl;
	cout << "-----------------------" << endl << endl;

	cout << "----- Wyniki RST: -----" << endl;
	cout << "RST y1 = " << RST1 << endl;
	cout << "RST y2 = " << RST2 << endl;
	cout << "RST y3 = " << RST3 << endl;
	cout << "-----------------------" << endl << endl;

	cout << "----- Wyniki RSC: -----" << endl;
	cout << "RSC y1 = " << RSC1 << endl;
	cout << "RSC y1 = " << RSC2 << endl;
	cout << "RSC y1 = " << RSC3 << endl;
	cout << "-----------------------" << endl << endl;

	cout << "----- Wyniki F2: ------" << endl;
	cout << "F2x = " << w_f2x << endl;
	cout << "F2y = " << w_f2y << endl;
	cout << "F2z = " << w_f2z << endl;
	cout << "-----------------------" << endl;
}

void POCH::START()
{
	OBLICZ();
}

int main()
{
	POCH poch;
	poch.START();
	cout << "\n\nWcisnij dowolny klawisz aby zakonczyc\n";
	_getch();
}