	#include "stdafx.h"
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <conio.h>
#include <iomanip>
#include <windows.h>

using namespace std;

class INTERPOLACJA_LINIOWA
{
	double x = 0, xi = 0, yi = 0, podanyx, wyn = 1;
	int ilosc, j;
	double * tabUi, *tabAi, ** tab;

	void Podaj_dane();
	void Wypisz_dane();
	void Oblicz();

public:
	void DZIALAJ();
};

void INTERPOLACJA_LINIOWA::Podaj_dane()
{
	cout << "Podaj ilosc wezlow: ";
	cin >> ilosc;

	tabUi = new double[ilosc];
	tabAi = new double[ilosc];

	tab = new double*[ilosc];
	for (j = 0; j < ilosc; j++) tab[j] = new double[2];

	cout << "Podaj wartosci Xi" << endl;
	for (j = 0; j < ilosc; j++) cin >> tab[j][0];

	cout << "Podaj wartosci Yi" << endl;
	for (j = 0; j < ilosc; j++)	cin >> tab[j][1];

	for (int j = 0; j < ilosc; j++) tabUi[j] = pow(tab[j][0], j);

	system("cls");
}
void INTERPOLACJA_LINIOWA::Wypisz_dane()
{
	cout << "Wpisane dane: \n\n";
	cout << "-----------------------------------------------";
	cout << "\ni: ";
	for (int j = 0; j < ilosc; j++)
	{
		cout << setw(7) << j;
	}
	cout << "\n-----------------------------------------------";
	cout << "\nXi:";
	for (int j = 0; j < ilosc; j++)
	{
		cout << setw(7) << tab[j][0];
	}
	cout << "\n-----------------------------------------------";
	cout << "\nYi:";
	for (int j = 0; j < ilosc; j++)
	{
		cout << setw(7) << tab[j][1];
	}
	cout << "\n-----------------------------------------------";
	cout << endl;
	cout << endl;

	cout << "POW:";
	for (int j = 0; j < ilosc; j++)
	{
		cout << setw(10) << tabUi[j];
	}
	cout << endl;
}
void INTERPOLACJA_LINIOWA::Oblicz()
{
	for (int j = 0; j < ilosc; j++)
	{
		if (j == 0)
			tabAi[j] = tab[j][1] / tabUi[j];
		else
			tabAi[j] = (tabUi[j] - tabUi[j - 1]) / tab[j][1];
	}
	cout << endl << endl;
	for (int j = 0; j < ilosc; j++)
	{
		cout << "a[" << j << "] = " << tabAi[j] << endl;
	}

	cout << "\n\nPodaj X: ";
	cin >> podanyx;

	for (j = podanyx; j > 0; j--)
	{
		wyn = wyn*podanyx + tabAi[j - 1];
	}
	cout << wyn;
}

void INTERPOLACJA_LINIOWA::DZIALAJ()
{
	Podaj_dane();
	Wypisz_dane();
	Oblicz();
}

int main()
{
	INTERPOLACJA_LINIOWA INT1;
	INT1.DZIALAJ();
	_getch();
}