// należy zwiększyć dokładnośc h do 0.0001

#include "stdafx.h"
#include <iostream>
#include <conio.h>

using namespace std;

class RR
{
	double h, x0, y0, x1, k1, k2, k3, k4, n, xn, ytemp, y;

	double f(double x, double y);
	void PodajDane();
	void Obliczenia();

public:
	void START();
};

int main()
{
	RR rr;
	rr.START();
	_getch();	
}

double RR::f(double x, double y)
{
	return x + y;
}

void RR::PodajDane()
{
	cout << "Podaj x0: ";
	cin >> x0;
	cout << "Podaj y0: ";
	cin >> y0;
	cout << "Podaj H: ";
	cin >> h;
	cout << "Podaj x1: ";
	cin >> x1;
}

void RR::Obliczenia()
{
	n = (abs(x1 - x0)) / h;
	ytemp = y0;
	for (int i = 1; i <= n; i++)
	{
		xn = x0 + i * h;
		k1 = h * f(xn, ytemp);
		k2 = h * f(xn + (1.0 / 2.0)*h, ytemp + (1.0 / 2.0) * k1);
		k3 = h * f(xn + (1.0 / 2.0)*h, ytemp + (1.0 / 2.0)*k2);
		k4 = h * f(xn + h, ytemp + k3);
		y = ytemp + (1.0 / 6.0)*(k1 + 2 * k2 + 2 * k3 + k4);
		ytemp = y;
	}
	cout << y;
}

void RR::START()
{
	PodajDane();
	Obliczenia();
}
