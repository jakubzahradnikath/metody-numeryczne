// INT3.cpp : Defines the entry point for the console application.
//
// Interpolacja3.cpp : Defines the entry point for the console application.
// 
// f(x) = 1/(1+x*x)     [-1,1]    n = 10
// alfa1 = 1/2
// beta1 = -1/2 
// pytamy o n
// h = (b-a)/n
// pytamy o x od 0 do 1
// m = (x-a)/h

#include "stdafx.h"
#include <iostream>


using namespace std;

double funkcja(double x)
{
	return 1 / (1 + x * x);
}

int main()
{
	double a = -1, b = 1, x, xm, h, m, t, alfa = 0.5, beta = -0.5, p;
	cout << "Podaj liczbe przedzialow: ";
	int n;
	cin >> n;
	cout << "Podaj x z przedzialu od " << a << " do " << b << " :";
	cin >> x;
	h = (b - a) / n;
	m = floor((x - a) / h);
	if (m == n)
		m = n - 1;
	xm = a + m * h;
	t = (x - xm) / h;

	double** tab = new double*[n];
	for (int i = 0; i < n; ++i)
		tab[i] = new double[4];

	double * bt = new double[n];
	double * g = new double[n];
	double * c = new double[n + 2];

	cout << "Podaj uklad (a, b, c, d): " << endl;
	tab[0][0] = 0;
	tab[0][1] = 4;
	tab[0][2] = 2;
	tab[0][3] = funkcja(a) + (1.0 / 3.0)*h*alfa;
	for (int i = 1; i < n - 1; i++)
	{
		tab[i][0] = 1;
		tab[i][1] = 4;
		tab[i][2] = 1;
		tab[i][3] = funkcja(a + i*h);
	}
	tab[n - 1][0] = 2;
	tab[n - 1][1] = 4;
	tab[n - 1][2] = 0;
	tab[n - 1][3] = funkcja(b) + (1.0 / 3.0)*h*beta;

	bt[0] = -tab[0][2] / tab[0][1];
	g[0] = tab[0][3] / tab[0][1];

	for (int i = 1; i < n; i++)
	{
		bt[i] = -tab[i][2] / (tab[i][0] * bt[i - 1] + tab[i][1]);
		g[i] = (tab[i][3] - tab[i][0] * g[i - 1]) / (tab[i][0] * bt[i - 1] + tab[i][1]);
	}

	c[n] = g[n];

	for (int i = n - 1; i >= 0; i--)
	{
		c[i + 1] = bt[i] * c[i + 1] + g[i];
	}
	c[0] = c[1] - (1.0 / 3.0) * h * alfa;
	c[n + 1] = c[n - 1] + (1.0 / 3.0) * h * beta;


	return 0;
}


